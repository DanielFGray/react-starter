# react-starter
My "boilerplate" for new React projects

## Features

* Component based UI with [React][react]
* Bundling with [Webpack 4][webpack]
* Transpiling with [Babel 7][babel]
  * linting with [eslint][eslint] and the [Airbnb config][airbnb]
* clean extensible CSS with [PostCSS][postcss] and [postcss-preset-env][postcss-env]
  * linting with [stylelint][stylint] and [stylelint-config-standard][stylconf]

[react]: https://reactjs.org
[webpack]: https://webpack.js.org
[babel]: https://babeljs.io
[eslint]: http://eslint.org/
[airbnb]: https://github.com/airbnb/javascript
[postcss]: http://postcss.org
[postcss-preset-env]: https://github.com/csstools/postcss-preset-env
[stylint]: https://stylelint.io
[stylconf]: https://github.com/stylelint/stylelint-config-standard
